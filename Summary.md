# FrameWork Angular
## Commandes
`ng new`: pour créer une nouvelle application Angular <br/>
`ng serve`: pour exécuter le serveur de développement <br/>
`ng generate component nom-du-component`: pour créer component
`ng build`: pour compiler Angular app


## Info
1. Lorsqu'on déclare un attribut de classe dans un fichier (.ts), on n'a pas
besoin de mot-clé : on le déclare simplement par son nom.

2.  Tous les components sont déclarés dans le fichier(app.module.ts).

3. La méthode  ngOnInit() elle permet d'initialiser des propriétés.

4. String interpolation: afficher le contenu d'une variable dans le template
(le nom de la variable à afficher entre doubles accolades {{ }})

5. attribute binding: permet de lier la valeur d'une variable à un attribut d'un élément du DOM
(avec Les crochets  [])

6. event binding: ermet de lier une méthode TypeScript à un événement du DOM.
(avec Les parenthèses  ())

7. On utilise le décorateur  @Input()  pour rendre injectable une propriété de component.

8. la directive  *ngIf  ajoute ou non un élément au DOM selon la condition.

9. La directive  *ngFor   permet d'insérer un élément dans le DOM pour chaque élément dans un tableau.

10. La directive  [ngStyle]  permet d'appliquer des styles.

11. La directive  [ngClass]  permet d'ajouter dynamiquement des classes à des éléments HTML.

12. DatePipe permet de formater les dates, et sans configuration fournit un formatage par défaut.

13. On peut créer un service avec le décorateur  @Injectable()  à une classe.

14. Pour ajouter des fichiers statiques à une application (comme des images), on les stocke dans le dossier  assets.

15. Pour ignorez l'activation des routes enfants c'est avec  [routerLinkActiveOptions]="{ exact: true }".

16. Pour naviguer vers une route absolue, il faut ajouter un  /  au début de la route demandée.

17. <router-outlet> permet de spécifie l'emplacement du component à afficher
