import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit ,Input} from '@angular/core';
import { Router } from '@angular/router';
import { FaceSnap } from '../models/face-snap.models';
import { FaceAppService } from '../service/face-app.service';

@Component({
  selector: 'app-face-snap',
  templateUrl: './face-snap.component.html',
  styleUrls: ['./face-snap.component.scss']
})
export class FaceSnapComponent implements OnInit{
  @Input() faceSnap!: FaceSnap;

  buttonText!: string;

  constructor(private faceAppService: FaceAppService, private router: Router){

  }

ngOnInit(){
  this.buttonText="like!"
}

onViewFaceSnap() {
  this.router.navigateByUrl(`facesnaps/${this.faceSnap.id}`);
}

addLike(){
  if(this.buttonText=="like!")
  {
    this.faceAppService.snapFaceSnapById(this.faceSnap.id,"like!");
    this.buttonText="dont like!";
  }
  else
  {
    this.faceAppService.snapFaceSnapById(this.faceSnap.id,"dont like!");
    this.buttonText="like!";
  }

}
}
