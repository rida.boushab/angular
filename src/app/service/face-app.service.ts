import { Injectable } from "@angular/core";
import { FaceSnap } from "../models/face-snap.models";

@Injectable({
    providedIn: "root"
})
export class FaceAppService{

    faceSnaps: FaceSnap[] = [
        {
        id: 1,
        title:"Summer trip 1",
        decription:"Summer in Santaroni",
        CreationDate: new Date(),
        likes:100,
        imageURL:"https://odyseasmarina.com/wp-content/uploads/2021/04/Santorini-53c9e0dca77b.jpeg",
        location:"Greece"

      },
      {
        id: 2,
        title:"Summer trip 2",
        decription:"Summer in Portugal",
        CreationDate:new Date(),
        likes:200,
        imageURL:"https://www.acs-ami.com/fr/blog/wp-content/uploads/2020/10/porto-portugal.jpg",
        location:"Porto"
      },
      {
        id: 3,
        title:"Summer trip 3",
        decription:"Summer in Florence",
        CreationDate:new Date(),
        likes:300,
        imageURL:"https://geo.img.pmdstatic.net/fit/https.3A.2F.2Fprd-dst-website-statics.2Es3.2Eeu-west-1.2Eamazonaws.2Ecom.2Fcontent.2Fuploads.2F2021.2F01.2Fflorence.2Ejpg/1800x675/background-color/ffffff/quality/70/picture.jpg"
      }
    ];

    getData(): FaceSnap[]{
        return this.faceSnaps;
    }
    FaceAppById(AppId: number): void{
        const faceSnap = this.faceSnaps.find(faceSnap => faceSnap.id === AppId);
        if(faceSnap){
            faceSnap.likes++;
        }
        else{
            throw new Error("not found");
        }
    }
    getFaceSnapById(faceSnapId: number): FaceSnap {
        const faceSnap = this.faceSnaps.find(faceSnap => faceSnap.id === faceSnapId);
        if (!faceSnap) {
            throw new Error('FaceSnap not found!');
        } else {
            return faceSnap;
        }
      }

    snapFaceSnapById(faceSnapId: number, snapType: 'like!' | 'dont like!'): void {
        const faceSnap = this.getFaceSnapById(faceSnapId);
        snapType === 'like!' ? faceSnap.likes++ : faceSnap.likes--;
    }
}
