import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FaceSnap } from '../models/face-snap.models';
import { FaceAppService } from '../service/face-app.service';

@Component({
  selector: 'app-single-app-face',
  templateUrl: './single-app-face.component.html',
  styleUrls: ['./single-app-face.component.scss']
})
export class SingleAppFaceComponent implements OnInit {

  @Input() faceSnap!: FaceSnap;

  buttonText!: string;

  constructor(private faceAppService: FaceAppService, private route: ActivatedRoute){

  }

ngOnInit(){
  this.buttonText="like!"
  const snapId = +this.route.snapshot.params['id'];
  this.faceSnap = this.faceAppService.getFaceSnapById(snapId);
}
addLike(){
  if(this.buttonText=="like!")
  {
    this.faceAppService.snapFaceSnapById(this.faceSnap.id,"like!");
    this.buttonText="dont like!";
  }
  else
  {
    this.faceAppService.snapFaceSnapById(this.faceSnap.id,"dont like!");
    this.buttonText="like!";
  }

}

}
